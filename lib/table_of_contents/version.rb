# frozen_string_literal: true

module Jekyll
  module TableOfContents
    VERSION = '0.16.0'
  end
end
